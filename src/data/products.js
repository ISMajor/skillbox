export default [
  {
    title: 'Coat for girls',
    price: 59,
    image: '/img/1.jpg',
  },
  {
    title: 'Checkered Dress',
    price: 12,
    image: '/img/3.jpg',
  },
  {
    title: 'Dress with a bow',
    price: 29,
    image: '/img/9.jpg',
  },
  {
    title: 'Princess Dress',
    price: 27,
    image: '/img/11.jpg',
  },
  {
    title: 'Ball dress',
    price: 28,
    image: '/img/12.jpg',
  },
  {
    title: 'Candy Dress',
    price: 20,
    image: '/img/13.jpg',
  },
  {
    title: 'Business suit',
    price: 65,
    image: '/img/15.jpg',
  },
  {
    title: "The Gentleman's Suit",
    price: 45,
    image: '/img/16.jpg',
  },
  {
    title: 'Denim Suit',
    price: 41,
    image: '/img/20.jpg',
  },

  {
    title: 'Kit for a boy',
    price: 18,
    image: '/img/30.jpg',
  },
  {
    title: '1Short fur coat',
    price: 39,
    image: '/img/36.jpg',
  },
  {
    title: 'Dance Dress1',
    price: 26,
    image: '/img/46.jpg',
  },
];
