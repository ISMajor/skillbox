import Vue from 'vue';
import App from './App.vue';

import data from './data';
import text from './text';

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount('#app');

text(data.message);
text(data.letter);
